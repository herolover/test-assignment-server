#include "utils.h"

#include <sstream>

std::vector<std::string> split(const std::string &str, char sym)
{
  std::stringstream stream(str);
  std::string token;
  std::vector<std::string> tokens;

  while (stream.good())
  {
    std::getline(stream, token, sym);
    tokens.push_back(token);
  }

  return tokens;
}
