#include "windows_service.h"

#include <thread>
#include <atomic>

std::string service_name;
SERVICE_STATUS service_status;
SERVICE_STATUS_HANDLE service_status_handle;

std::function<void()> on_start;
std::function<void()> process;
std::function<void()> on_stop;
std::atomic_bool is_run;

void set_service_status(DWORD current_state, DWORD controls_accepted = 0,
                        DWORD exit_code = NO_ERROR, DWORD wait_hint = 0)
{
  service_status.dwControlsAccepted = controls_accepted;
  service_status.dwCurrentState = current_state;
  service_status.dwWin32ExitCode = exit_code;
  service_status.dwWaitHint = wait_hint;
  service_status.dwCheckPoint = 0;

  SetServiceStatus(service_status_handle, &service_status);
}

VOID WINAPI service_control_handler(DWORD control_code)
{
  if (control_code == SERVICE_CONTROL_STOP)
  {
    is_run = false;
    set_service_status(SERVICE_STOP_PENDING);
  }
}

VOID WINAPI service_main(DWORD argc, LPTSTR *argv)
{
  service_status_handle = RegisterServiceCtrlHandler(service_name.c_str(), service_control_handler);
  if (service_status_handle == nullptr)
    return;

  service_status.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
  service_status.dwServiceSpecificExitCode = 0;

  set_service_status(SERVICE_START_PENDING);
  on_start();
  set_service_status(SERVICE_RUNNING, SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN);

  is_run = true;
  while (is_run)
    process();

  set_service_status(SERVICE_STOP_PENDING);
  on_stop();
  set_service_status(SERVICE_STOPPED);
}

DWORD run_service(const std::string &service_name,
                  const std::function<void()> &on_start,
                  const std::function<void()> &process,
                  const std::function<void()> &on_stop)
{
  ::service_name = service_name;
  ::on_start = on_start;
  ::process = process;
  ::on_stop = on_stop;

  SERVICE_TABLE_ENTRY service_table[] =
  {
    {"", service_main},
    {nullptr, nullptr}
  };

  if (StartServiceCtrlDispatcher(service_table) == FALSE)
    return GetLastError();

  return 0;
}