#ifndef BANK_H
#define BANK_H

#include <vector>
#include <string>
#include <ostream>
#include <chrono>

#include "fixed_point_number.h"

class Bank
{
public:
  struct Operation
  {
    enum Type
    {
      BUY,
      SELL,
      TYPE_COUNT
    };

    enum Status
    {
      CONFIRM,
      PARTIALLY_CONFIRM,
      REJECT,
      STATUS_COUNT
    };

    Type type;
    unsigned volume;
    unsigned symbol_id;
    Price price;
    Status status;
  };

  Bank(const std::vector<std::string> &symbols);
  void update_quotes();
  Operation make_operation();

  inline
  bool is_time_to_update_quotes() const
  {
    return std::chrono::steady_clock::now() >= _time_to_update_quotes;
  }

  inline
  bool is_time_to_make_operation() const
  {
    return std::chrono::steady_clock::now() >= _time_to_make_operation;
  }

  inline
    const std::vector<Price> & quotes() const
  {
    return _quotes;
  }
private:
  void update_time_to_update_quotes();
  void update_time_to_make_operation();

  std::vector<std::string> _symbols;

  std::chrono::steady_clock::time_point _time_to_update_quotes;
  std::chrono::steady_clock::time_point _time_to_make_operation;

  std::vector<Price> _quotes;
};

std::ostream & operator<<(std::ostream &stream, const Bank::Operation &operation);

#endif // BANK_H
