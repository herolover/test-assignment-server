#pragma once

#include <ostream>
#include <string>

inline
long long pow10(unsigned pow)
{
  return pow == 0 ? 1 : 10 * pow10(pow - 1);
}

template<unsigned Digits>
class FixedPointNumber
{
private:
  long long _number;
public:
  FixedPointNumber()
    : _number(0)
  {
  }

  FixedPointNumber(const std::string & number)
  {
    std::size_t pos = number.find('.');
    if (pos != std::string::npos)
    {
      std::size_t fraction_digits = number.size() - pos - 1;
      if (fraction_digits > Digits)
        fraction_digits = Digits;

      _number = std::stoll(number.substr(0, pos)) * pow10(Digits) + std::stoll(number.substr(pos + 1)) * pow10(Digits - fraction_digits);
    }
    else
      _number = std::stoll(number);
  }

  FixedPointNumber<Digits> operator-() const
  {
    return FixedPointNumber<Digits>(-_number);
  }

  FixedPointNumber<Digits> operator+(const FixedPointNumber<Digits> &number) const
  {
    FixedPointNumber<Digits> res(*this);
    res += number;

    return res;
  }

  FixedPointNumber<Digits> & operator+=(const FixedPointNumber<Digits> &number)
  {
    _number += number._number;

    return *this;
  }

  FixedPointNumber<Digits> operator-(const FixedPointNumber<Digits> &number) const
  {
    FixedPointNumber<Digits> res(*this);
    res -= number;

    return res;
  }

  FixedPointNumber<Digits> & operator-=(const FixedPointNumber<Digits> &number)
  {
    _number -= number._number;

    return *this;
  }

  FixedPointNumber<Digits> operator*(const FixedPointNumber<Digits> &number) const
  {
    FixedPointNumber<Digits> res(*this);
    res *= number;

    return res;
  }

  FixedPointNumber<Digits> operator*=(const FixedPointNumber<Digits> &number)
  {
    _number *= number._number;
    _number /= pow10(Digits);

    return *this;
  }

  std::string as_string() const
  {
    std::string number = std::to_string(_number / pow10(Digits)) + "." + std::string(Digits, '0');
    std::string fraction = std::to_string(std::abs(_number) % pow10(Digits));
    number.replace(number.size() - fraction.size(), std::string::npos, fraction);
    return number;
  }
};

template<unsigned Digits>
std::ostream & operator<<(std::ostream &stream, const FixedPointNumber<Digits> &number)
{
  stream << number.as_string();

  return stream;
}
