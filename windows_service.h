#ifndef WINDOWS_SERVICE
#define WINDOWS_SERVICE

#include <windows.h>

#include <functional>
#include <string>

DWORD run_service(const std::string &service_name,
                  const std::function<void()> &on_start,
                  const std::function<void()> &process,
                  const std::function<void()> &on_stop);

#endif