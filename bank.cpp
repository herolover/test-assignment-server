#include "bank.h"

#include <random>

std::random_device rd;
std::mt19937 gen(rd());

Bank::Bank(const std::vector<std::string> &symbols)
  : _symbols(symbols)
{
  std::uniform_real_distribution<> random(1.0, 10.0);

  for (auto &symbol: symbols)
    _quotes.push_back(Price(random(gen)));

  update_time_to_update_quotes();
  update_time_to_make_operation();
}

void Bank::update_quotes()
{
  std::uniform_real_distribution<> real_random(-0.1, 0.1);

  for (auto &quote : _quotes)
  {
    quote += Price(real_random(gen));
    if (quote.as_double() < 0.0)
      quote = -quote;
  }

  update_time_to_update_quotes();
}

Bank::Operation Bank::make_operation()
{
  std::uniform_int_distribution<> int_random(100, 1000);

  Bank::Operation operation;
  operation.type = (Operation::Type)(int_random(gen) % Operation::TYPE_COUNT);
  operation.volume = int_random(gen);
  operation.symbol_id = int_random(gen) % _quotes.size();
  operation.price = _quotes[operation.symbol_id] * operation.volume;
  operation.status = (Operation::Status)(int_random(gen) % Operation::STATUS_COUNT);

  update_time_to_make_operation();

  return operation;
}

void Bank::update_time_to_update_quotes()
{
  std::uniform_int_distribution<> time_random(100, 1000);
  _time_to_update_quotes = std::chrono::steady_clock::now() + std::chrono::milliseconds(time_random(gen));
}

void Bank::update_time_to_make_operation()
{
  std::uniform_int_distribution<> time_random(1000, 5000);
  _time_to_make_operation = std::chrono::steady_clock::now() + std::chrono::milliseconds(time_random(gen));
}

std::ostream &operator<<(std::ostream &stream, const Bank::Operation &operation)
{
  stream << operation.type << " "
         << operation.volume << " "
         << operation.symbol_id << " "
         << operation.price << " "
         << operation.status;

  return stream;
}
