#include <thread>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>

#include <direct.h>

#include "ini-parser\INIReader.h"
#include "zmq.h"

#include "windows_service.h"
#include "bank.h"
#include "utils.h"

std::string exe_directory;
INIReader *ini;

std::vector<Bank> banks;

void *context;
zmq_pollitem_t pollitems[2];

void on_start()
{
  ini = new INIReader(exe_directory + "config.ini");

  auto symbols = split(ini->Get("data", "symbols", ""), ',');
  auto bank_names = split(ini->Get("data", "banks", ""), ',');
  for (std::size_t i = 0; i < bank_names.size(); ++i)
    banks.push_back(Bank(symbols));

  context = zmq_ctx_new();
  pollitems[0].socket = zmq_socket(context, ZMQ_REP);
  pollitems[1].socket = zmq_socket(context, ZMQ_PUB);
  zmq_bind(pollitems[0].socket, ("tcp://*:" + ini->Get("server", "rep_port", "59152")).c_str());
  zmq_bind(pollitems[1].socket, ("tcp://*:" + ini->Get("server", "pub_port", "59153")).c_str());
  pollitems[0].events = ZMQ_POLLIN;
  pollitems[1].events = ZMQ_POLLOUT;
}

void process()
{
  if (zmq_poll(pollitems, 2, 0) > 0)
  {
    if (pollitems[0].revents & ZMQ_POLLIN)
    {
      zmq_msg_t msg;
      zmq_msg_init(&msg);
      zmq_msg_recv(&msg, pollitems[0].socket, ZMQ_DONTWAIT);

      std::string request((char *)zmq_msg_data(&msg), zmq_msg_size(&msg));
      std::string response;
      if (request == "get-symbols")
        response = ini->Get("data", "symbols", "");
      else if (request == "get-banks")
        response = ini->Get("data", "banks", "");

      zmq_send(pollitems[0].socket, response.c_str(), response.size(), 0);
    }

    if (pollitems[1].revents & ZMQ_POLLOUT)
    {
      for (std::size_t i = 0; i < banks.size(); ++i)
      {
        if (banks[i].is_time_to_update_quotes())
        {
          banks[i].update_quotes();
          auto &quotes = banks[i].quotes();

          std::stringstream stream;
          stream << "q " << i;
          for (auto &quote : quotes)
            stream << " " << quote;

          std::string message = stream.str();
          zmq_send(pollitems[1].socket, message.c_str(), message.length(), 0);
        }

        if (banks[i].is_time_to_make_operation())
        {
          std::stringstream stream;
          stream << "o " << i << " " << banks[i].make_operation();

          std::string message = stream.str();
          zmq_send(pollitems[1].socket, message.c_str(), message.length(), 0);
        }
      }
    }
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

void on_stop()
{
  delete ini;

  std::string message = "close";
  zmq_send(pollitems[1].socket, message.c_str(), message.length(), 0);

  zmq_close(pollitems[0].socket);
  zmq_close(pollitems[1].socket);
  zmq_ctx_destroy(context);
}

int main(int argc, char *argv[])
{
  std::vector<std::string> args;
  for (int i = 0; i < argc; ++i)
    args.push_back(std::string(argv[i]));

  exe_directory = args[0].substr(0, args[0].find_last_of('\\') + 1);

  std::string service_name("test-assignment-server");

  char current_dir[255];
  _getcwd(current_dir, 255);

  std::unordered_map<std::string, std::string> commands =
  {
    {"-create", "sc create " + service_name + " binPath=\"" + current_dir + "\\" + args[0] + "\""},
    {"-delete", "sc delete " + service_name},
    {"-start", "sc start " + service_name},
    {"-stop", "sc stop " + service_name}
  };

  if (args.size() > 1 && commands.count(args[1]) == 1)
    system(commands[args[1]].c_str());
  else
  {
    DWORD result = run_service(service_name, on_start, process, on_stop);

    if (result == ERROR_FAILED_SERVICE_CONTROLLER_CONNECT)
    {
      std::cout << "Commands: " << std::endl;
      for (auto &command : commands)
        std::cout << " " << command.first << std::endl;
    }

    return result;
  }
}